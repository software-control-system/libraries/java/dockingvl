/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.vl;

import java.util.HashMap;
import java.util.Map;

import com.vldocking.swing.docking.Dockable;
import com.vldocking.swing.docking.DockableState;
import com.vldocking.swing.docking.DockableState.Location;
import com.vldocking.swing.docking.DockingDesktop;
import com.vldocking.swing.docking.RelativeDockablePosition;
import com.vldocking.swing.docking.event.DockableSelectionEvent;
import com.vldocking.swing.docking.event.DockableSelectionListener;
import com.vldocking.swing.docking.event.DockingActionCloseEvent;
import com.vldocking.swing.docking.event.DockingActionEvent;
import com.vldocking.swing.docking.event.DockingActionListener;

import fr.soleil.docking.vl.view.VlDockView;

public class SoleilDockingDesktop extends DockingDesktop {

    private static final long serialVersionUID = -6192752805034560434L;

    protected final Map<String, RelativeDockablePosition> closedPositions;

    protected boolean closeEnabled;
    protected boolean autoHideEnabled;
    protected boolean maximizeEnabled;
    protected boolean floatingEnabled;

    public SoleilDockingDesktop() {
        this(true, false, true, false);
    }

    public SoleilDockingDesktop(boolean close, boolean autoHide, boolean maximize, boolean floating) {
        super();
        closedPositions = new HashMap<>();
        closeEnabled = close;
        autoHideEnabled = autoHide;
        maximizeEnabled = maximize;
        floatingEnabled = floating;
        addDockingActionListener(generateDockingActionListener());
        addDockableSelectionListener(new DockableSelectionListener() {
            @Override
            public void selectionChanged(DockableSelectionEvent dse) {
                if (dse.getSelectedDockable() instanceof VlDockView) {
                    ((VlDockView) dse.getSelectedDockable()).focusGained(null);
                }
            }
        });
    }

    @Override
    public void addDockable(Dockable dockable) {
        if (dockable != null) {
            // DockKey key = dockable.getDockKey();
            // key.setCloseEnabled(closeEnabled);
            // key.setAutoHideEnabled(autoHideEnabled);
            // key.setMaximizeEnabled(maximizeEnabled);
            // key.setFloatEnabled(floatingEnabled);

            addDockingActionListener((VlDockView) dockable);
            super.addDockable(dockable, new RelativeDockablePosition(1, 0, 0.5, 0.5));
        }
    }

    private DockingActionListener generateDockingActionListener() {
        DockingActionListener result = new DockingActionListener() {
            @Override
            public boolean acceptDockingAction(DockingActionEvent event) {
                return true;
            }

            @Override
            public void dockingActionPerformed(DockingActionEvent event) {
                if (event.getActionType() == DockingActionEvent.ACTION_CLOSE) {
                    Dockable closedDockable = ((DockingActionCloseEvent) event).getDockable();
                    DockableState state = getDockableState(closedDockable);
                    RelativeDockablePosition position = state.getPosition();
                    closedPositions.put(closedDockable.getDockKey().getKey(), position);
                }
            }
        };
        return result;
    }

    public void showDockable(Dockable dockable) {
        DockableState state = getDockableState(dockable);
        if (state == null || Location.CLOSED.equals(state.getLocation())
                || Location.HIDDEN.equals(state.getLocation())) {
            RelativeDockablePosition position = getPositionForClosedView(dockable);
            if (position == null) {
                addDockable(dockable);
            } else {
                addDockable(dockable, position);
            }
        }
    }

    private RelativeDockablePosition getPositionForClosedView(Dockable dockable) {
        RelativeDockablePosition result = null;
        if (dockable != null) {
            result = closedPositions.get(dockable.getDockKey().getKey());
        }
        return result;
    }

}
