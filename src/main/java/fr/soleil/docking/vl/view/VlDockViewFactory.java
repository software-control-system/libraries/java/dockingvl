/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.vl.view;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JComponent;

import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.view.AbstractViewFactory;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.vl.SoleilDockingDesktop;
import fr.soleil.docking.vl.VlDockDockingManager;

public class VlDockViewFactory extends AbstractViewFactory {

    public VlDockViewFactory() {
        super();
    }

    @Override
    protected IView createView(String title, Icon icon, Component component, Object id) {
        IView result = new VlDockView(title, icon, component, id);
        return result;
    }

    @Override
    protected void updateViewForDockingArea(IView view, JComponent dockingArea) {
        if ((view instanceof VlDockView) && (dockingArea instanceof SoleilDockingDesktop)) {
            VlDockView vlDockView = (VlDockView) view;
            SoleilDockingDesktop dockingDesktop = (SoleilDockingDesktop) dockingArea;
            dockingDesktop.addDockable(vlDockView);
            vlDockView.setDockingDesktop(dockingDesktop);
        }
    }

    @Override
    public ADockingManager generateDockingManager() {
        return new VlDockDockingManager(this);
    }

}
